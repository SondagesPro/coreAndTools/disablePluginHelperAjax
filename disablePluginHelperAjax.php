<?php
/**
 * Disable PluginHelper::ajax function
 *
 * @author Denis Chenu <denis@sondages.pro>
 * @copyright 2019 Denis Chenu <http://www.sondages.pro>
 * @license MIT
 * @version 0.1.0
 * @see https://bugs.limesurvey.org/view.php?id=14997
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 **/
class disablePluginHelperAjax extends PluginBase
{

    static protected $name = 'disablePluginHelperAjax';
    static protected $description = 'Disable PluginHelper::ajax function, add an event for allowed plugin::method';

    public function init()
    {
        $this->subscribe('beforeControllerAction');
        if(YII_DEBUG) {
            $this->subscribe('beforePluginHelperAjax');
        }
    }
    /**
     * @see https://manual.limesurvey.org/BeforeControllerAction
     * Disable PluginHelper::ajax before it happen
     * Allow to add plugin:method using beforePluginHelperAjax event
     * With append ['pluginName','pluginMethod'] to allowedPluginMethod event param
     * Warning : plugin using afterPluginLoad and beforeControllerAction are not protected
     * This plugin throw error only after event happen
     * @return void
     * @throws CHttpException default invalid request from Yii
     */
    public function beforeControllerAction()
    {
        $coreEvent = $this->getEvent();
        $controller = $coreEvent->get('controller');
        $action = $coreEvent->get('action');
        $subaction = $coreEvent->get('subaction');
        if($controller != 'admin' || $action != 'pluginhelper' || $subaction != 'ajax') {
            return;
        }
        $plugin = Yii::app()->getRequest()->getParam('plugin');
        $method = Yii::app()->getRequest()->getParam('method');
        $thisEvent = new PluginEvent('beforePluginHelperAjax');
        /* Array [[PluginName,methodName]] */
        $thisEvent->set('allowedPluginMethod', array());
        App()->getPluginManager()->dispatchEvent($thisEvent);
        $allowedPluginMethod=$thisEvent->get('allowedPluginMethod');
        if(empty($allowedPluginMethod) || !is_array($allowedPluginMethod)) {
            throw new \CHttpException(400,Yii::t('yii','Your request is invalid.'));
        }
        if(!in_array(array($plugin,$method),$allowedPluginMethod)) {
            throw new \CHttpException(400,Yii::t('yii','Your request is invalid.'));
        }
    }

    /**
     * Sample to allow a specific plugin method for PluginHelper::ajax
     * @return @void
     */
    public function beforePluginHelperAjax()
    {
        $allowedPluginMethod = array(
            array('disablePluginHelperAjax','checkAllowedMethod')
        );
        if(method_exists($this->getEvent(),'append')) {
            $this->getEvent()->append('allowedPluginMethod', $allowedPluginMethod);
        } else {
            $allowedPluginMethods=(array)$this->event->get('allowedPluginMethod');
            $allowedPluginMethods=array_merge($allowedPluginMethods,$allowedPluginMethod);
            $this->event->set('allowedPluginMethod',$allowedPluginMethods);
        }
    }
    /**
     * Sample allowed function
     * @return @void
     */
    public function checkAllowedMethod()
    {
        echo "It‘s allowed";
    }
    /**
     * Sample disallowed function
     * @return @void
     */
    public function checkNotAllowedMethod()
    {
        echo "It‘s not allowed";
    }

}
